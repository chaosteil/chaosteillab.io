title: Setting up a simple Conan project
tags:
  - c++
  - conan
  - cmake
  - catch
categories: []
date: 2016-04-12 22:49:00
---
I've recently been doing some more things with C++ again, and I've come to use a specific project structure which I will write about here. Package mangement is a bit foreign to the C/C++ world, but I wanted to integrate it for my projects. Not only simply because I could, but also because I really want to see it take off a little, possibly contribute if I spot a few issues here and there. On the other hand, it's really handy to have specific versions of libraries that are confirmed to be working with your source always handy in a specific repository. That of course is kind of the main reason why package managers exist, and to elaborate further would move me a bit off track.

The whole sample project can be found on [github] as well.

<!-- more -->

# Tools
For C++ there is this package manager [conan.io], which allows you to use packages for your C++ projects. It's a great package manager by design, as it properly namespaces projects with authors and release channels, so multiple projects can share the same name without conflicting. Conan is probably easiest to integrate when you're just starting out with a new project, but I would not be able to comment on how it works for more advanced projects.

To install Conan for Linux (if you have python installed), you simply run
```bash
pip install conan
```

To build the whole project, I will also be using [CMake], which is a cross-platform build tool manager. It's really easy to integrate it with conan, sort of like a natural extension. To install it, you should use your system-provided package manager.

And, at last, to test this thing I'm building, I'm using [Catch], which is a nice C++11 testing framework, that works in a very natural C++-ish way. In a way, all these helper tools start with `C`, which is a happy coincidence. I'll be installing this library with conan itself.

# The project
First of all, I generate a basic structure for my project, this usually looks something like this:
```txt
|-- src/
|  `-- main.cpp
|-- tests/
|  `-- test.cpp
|-- LICENSE
|-- README.md
```

We fill the `main.cpp` file with some sample code:
```c++
#include <iostream>

int main() {
  std::cout << "Hello world!\n";
}
```

And the `test.cpp` file with:
```c++
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
```

The license and readme should be filled accordingly!

## conanfile.py
First, we add our `conanfile.py` to the root of the project. This will store our dependencies and additional package settings. For simple projects, it really does not have to be complicated, but I will at least have [Catch] as a dependency, that conan will immediately pull:
```python
from conans import ConanFile, CMake
import traceback

# Just replace `MySimpleProject` with your name
class MySimpleProject(ConanFile):
    name = "MySimpleProject"
    version = "0.1"
    settings = "os", "compiler", "build_type", "arch"
    requires = "catch/1.3.0@TyRoXx/stable"
    generators = "cmake"
```

## CMakeLists.txt
Now we need to tell our system how to actually build our little project. Since we set conan to build with CMake, we can use both tools together to let CMake manage the package management. This means, we add some conan-specific includes and libraries into our `CMakeLists.txt`, which we'll place into the root folder. We'll also set it to use the latest C++ standard and pick up all `*.cpp` files from our `src/` directory automatically.

```cmake
project(mysimpleproject)
cmake_minimum_required(VERSION 3.0)
# C++14 because we're "edgy".
set(CMAKE_CXX_STANDARD 14)

# Setup conan and include everything.
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

# Include the src directory, so we can freely include our headers from there.
include_directories(src)
# Grab all *.cpp files recursively.
file(GLOB_RECURSE PROJECT_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")

# Exclude main file for library generation, only use it in the final
# executable. This lets us test everything without compiling twice.
set(PROJECT_MAIN "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp")
list(REMOVE_ITEM PROJECT_SOURCE ${PROJECT_MAIN})

# Build our project with the help of conan.
include_directories(${CONAN_INCLUDE_DIRS})
add_library(mysimpleprojectlib STATIC ${PROJECT_SOURCE})
add_executable(mysimpleproject ${PROJECT_MAIN})
target_link_libraries(mysimpleproject mysimpleprojectlib ${CONAN_LIBS})

# Now enable our tests.
enable_testing()
add_subdirectory(tests)
```

As you can see, we're building the whole project into a static library, except main.cpp. This allows us to have two entry points for the same object files, which in turn allows us to test the project without recompiling everything for the test itself. Both the main binary and the test binary only need to link against the static library.

This brings me to the test `CMakeLists.txt`, which should be saved into the `tests/` directory. It's a slightly simpler form our our main `CMakeLists.txt` file, as we took care of most of the settings already:
```cmake
cmake_minimum_required(VERSION 3.0)

file(GLOB_RECURSE TEST_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_executable(mysimpleproject_test ${TEST_SOURCE})
target_link_libraries(mysimpleproject_test mysimpleprojectlib ${CONAN_LIBS})
# We run our test from the source directory, so we can consistently
# load files with known paths.
# I also like color in my verbose output, so I put in a --force-colour flag.
add_test(NAME mysimpleproject_test
         WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
         COMMAND ${CMAKE_BINARY_DIR}/bin/mysimpleproject_test --force-colour)
```

For now, while `main.cpp` is our only source file, we should also add a dummy file (which we'll remove later once we have at least one other `*.cpp` file), so our build has at least a single object to compile with the static library. We can do this simply by doing
```bash
echo "//Dummy file" > src/lib.cpp
```

This should be it! This project should now be buildable!

# Build the project
We should of course build our project out-of-source, thus we make a `build/` directory in root, and change into it:
```bash
mkdir build && cd build
```
Now we can easily use conan to fetch our packages and generate all the necessary files in the current directory, almost by magic:
```bash
conan install ..
```
Bam! Beautiful. After conan is done with all the fetching, it will have generated a few files into our `build/` directory, which will then be used by CMake. Which of course will get called like this:
```bash
cmake ..
```
Now everything should be ready to actually build the project. We can either use just `make` (since that's the default on Linux), or be more *cross-platformey* by using:
```bash
cmake --build .
```
This should've built our binary and test binary in `build/bin/`! To run tests automatically, you should run:
```bash
ctest -V # This will force output from Catch to be shown in the console
```

# Done!
This is our final directory structure:
```txt
|-- build/
|   `-- ...
|-- src/
|  `-- main.cpp
|-- tests/
|  |-- CMakeLists.txt
|  `-- test.cpp
|-- CMakeLists.txt
|-- conanfile.py
|-- LICENSE
|-- README.md
```

Don't forget to look into [Catch], so you test your project thoroughly. This initial configuration will help you get into it without any further issues. If more packages get added to the `conanfile.py`, the tests will also automatically pick up on them.

If you don't want to mess with all this manually, feel free to fork my sample repo on [github].

## .gitignore
Just as a little bonus, here is my sample `.gitignore` that I use with this setup:
```txt
build/
*.pyc
```

Exciting, isn't it? Since we're putting all our build files into our `build/` directory, we only have to ignore that, as well as the occasional garbage generated from the `conanfile.py` parse, which isn't an issue.

[github]: https://github.com/Chaosteil/sample
[conan.io]: https://www.conan.io/
[cmake]: https://cmake.org/
[catch]: https://github.com/philsquared/Catch
