title: GeeBee Gameboy Emulator written in C++
tags:
  - geebee
  - gameboy
  - emulator
  - c++
date: 2016-04-09 16:40:00
---
About a few weeks back I decided to just finally start writing a Gameboy emulator, as that's something I've always wanted to do. Since I've gotten a lil' rusty with my C++ (especially with C++11(+)) I decided to use some of the tools I've read so much about, but was not able to use them till now.

Here's a little spoiler of the final result:

{% limg geebee_rtype.png %}
And the source can be found on [github] or [gitlab].

<!-- more -->

# Setup
First of all, I wanted to use a package manager for my project, so I could easily set it up on any platform without experiencing build system hell (at least in theory), so I chose [conan], which is a spiritual successor to biicode.

[Conan] supports [Cmake] as a build system, and as that being cross platform, I just went with it. The actual setup of conan was really easy, as you simply have to setup a `conanfile.py` with some settings and the library your project needs to pull from conan, and that's it. Conan didn't had a [SDL] library at that point, and [SFML] didn't work so well in my development VM, so I went ahead and made [one][conansdl] (There is a better, more official one currently, so feel free to ignore mine). Making one was actually fairly easy, and with a few magic [tricks][conan-package-tools], I could get Travis CI to build several packages for various platforms and upload them automatically.

# Other Tools
Some other tools that are super useful for this, are of course the [Clang Static Analysis][clang-ca] tools, [perf] for performance issues, and [Clang Format][clang-format] to keep the code nice and tidy. I always treat my warnings as errors, with as many warnings as possible, and so should you!

# Emulator
With the boilerplate out of the way came the actual emulator writing stuff. The Gameboy Emulator community apparently has lots of great resources that help you writing things like that! I've used mostly these documents to get through the hoops of instructions, their timings and other things:

- [GB CPU Manual]
- [GB Opcodes Table]
- [Pandocs]

These are actually aimed at developers writing assembler code for the gameboy, but of course with a bit of reading around it can also be abused for actual emulator writing.

Of course there being just written material isn't enough, there are also great [Test ROMs] written by the community, which I could also use to test how my actual instructions worked. 

These were immensely helpful in finding actual bugs and misbehaviors in my code. After I was confident they worked well, I also was able to integrate them into CI with [Catch].

# LCD
Before I started writing the emulator, I always thought that the LCD driver reads directly from Video RAM pixel-per-pixel. Oh boy, how wrong I was!

The VRAM is actually storing 192 tiles, which are read from on a per-scanline basis, from various tilemaps. There are tilemaps for the background and sprites, and depending on where all the objects are positioned, it will draw specific pixels on lines. The game programmer could even, while the driver does short breaks between lines, change some values around and have the next line be completely different. Replicating and getting your head around this was certainly... odd.

The LCD driver is still the component I am least comfortable with, as there aren't actually any good tests, and writing tests that properly verify correct drawings of sprite, background and window information is more of a trouble than I'm willing to bother with. It's also a bit shoddily written, but it does it's job.

# Future Work
I'm currently happy with how it works, but there is a lot of stuff still to be done. There is no sound support, no custom key input, and support for various memory bank controllers is just in theory, with only MBC1 really working for sure. I've also not really documented the code, so that's something I should really look out for. I also wish I could've done more C++11(+) specific stuff, but that might come in time.

In any case, thanks for reading this, and feel free to give me pull requests on [github] or [gitlab]!

[conan]: https://www.conan.io/
[cmake]: https://cmake.org/
[catch]: https://github.com/philsquared/Catch
[sdl]: https://www.libsdl.org/
[sfml]: http://www.sfml-dev.org/
[conansdl]: https://github.com/Chaosteil/conan-sdl
[conan-package-tools]: https://github.com/conan-io/conan-package-tools
[clang-ca]: http://clang-analyzer.llvm.org/scan-build.html
[clang-format]: http://clang.llvm.org/docs/ClangFormat.html
[perf]: https://perf.wiki.kernel.org/index.php/Main_Page
[gb cpu manual]: http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf
[gb opcodes table]: http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
[test roms]: http://gbdev.gg8.se/wiki/articles/Test_ROMs
[pandocs]: http://bgb.bircd.org/pandocs.htm
[gitlab]: https://gitlab.com/chaosteil/geebee
[github]: https://github.com/Chaosteil/geebee